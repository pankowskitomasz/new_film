import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MovieRoutingModule } from './movie-routing.module';
import { MovieComponent } from './movie/movie.component';
import { MoviebarComponent } from './moviebar/moviebar.component';
import { MovielistComponent } from './movielist/movielist.component';


@NgModule({
  declarations: [
    MovieComponent,
    MoviebarComponent,
    MovielistComponent
  ],
  imports: [
    CommonModule,
    MovieRoutingModule
  ]
})
export class MovieModule { }
