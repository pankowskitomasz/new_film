import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-moviebar',
  templateUrl: './moviebar.component.html',
  styleUrls: ['./moviebar.component.sass']
})
export class MoviebarComponent implements OnInit {
  showFilters:boolean=true;
  
  showHideFilters(){
    this.showFilters=!this.showFilters;
  }
  constructor() { }

  ngOnInit(): void {
  }

}
