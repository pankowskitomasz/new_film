import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TicketRoutingModule } from './ticket-routing.module';
import { TicketComponent } from './ticket/ticket.component';
import { TicketbarComponent } from './ticketbar/ticketbar.component';
import { TicketlistComponent } from './ticketlist/ticketlist.component';


@NgModule({
  declarations: [
    TicketComponent,
    TicketbarComponent,
    TicketlistComponent
  ],
  imports: [
    CommonModule,
    TicketRoutingModule
  ]
})
export class TicketModule { }
