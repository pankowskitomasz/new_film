import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TicketbarComponent } from './ticketbar.component';

describe('TicketbarComponent', () => {
  let component: TicketbarComponent;
  let fixture: ComponentFixture<TicketbarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TicketbarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TicketbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
