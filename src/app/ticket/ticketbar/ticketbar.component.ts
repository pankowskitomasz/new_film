import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ticketbar',
  templateUrl: './ticketbar.component.html',
  styleUrls: ['./ticketbar.component.sass']
})
export class TicketbarComponent implements OnInit {
  showFilters:boolean=true;
  
  showHideFilters(){
    this.showFilters=!this.showFilters;
  }
  constructor() { }

  ngOnInit(): void {
  }

}
